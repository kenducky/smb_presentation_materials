# SMB Presentation Script

## Title Slide
- Introduce myself, institution (Engineer with a sledgehammer)
- Supervisors

## Slide 2 (Motivation)
- Read motivations slide
- Misdiagnosis, operational efficiency

## Slide 3 (Overview)
- Modelling
  - 3D ODE model for pathogen level, immune response level, health measure
  - Reproduce empirical conjecture by Schneider
- Parameterisation
  - Estimation of parameters for this model on mice mode of malaria
  - Small data set

## Slide 4 (Model)

## Slide 5 (System in Literature)
- Empirical conjecture of possible dynamics of pathogen and health level
- Thinking about recovery dynamics within host
- Listing the things we see in pathogenic space - common sense
  - Recovery or death
  - Full or partial Recovery
  - Death before and after clearance
  - Thinking about other factors, like the immune system as a driver of bad health

## Slide 6 (Proposed Model)

## Slide 7 (Proposed Model)
- Note that we are building on an existing model of the immune response model in 1997 Mayer et al.
- Note that health term is decoupled, for analysis and intuition

## Slide 8 (Proposed Model)
- Go through terms
- Notes
  - Response and Auto-catalysis terms are over-simplifications
    - Follow a logistic response Model
      - Implies that the response rate is capped (physical limitations)
  - Weird positive feedback mechanism in the fyz term
- Behaviours next slide - after removing some terms that define the 2D model, or can be rescaled

## Slide 9 (Classification Diagram)
- Looking at the behaviours for a fixed set of parameters
- Show reproduction of terms
- Remember that (5) is literally any of (A-D) since that's just drawing a threshold line

- Note that (C) is not a behaviour we expect to ever see - but it happens so there must be some underlying thing here that we have not modelled

## Slide 10 (Classification Diagram)
- Horizontal axis is determined by the size of the initial load - behaviour inherited from the Mayer model
- Vertical axis is determined by a ratio of three things
  - f/(gy*)
    - y* is the equilibrium solution to the second equation with x -> 0
  - It's the impact of the immune response relative to the ability for the health to self-recover and level of immune response

## Slide 11 (Fit)
- Think about validating the model, and seeing how useful it is for individuals

## Slide 12 (Case Study)
- Mouse model of malaria (I've heard it's contentious)
- A short-term study of how mice recover after being exposed to malaria
  - by B.Y. Torres
  - [Parasite density] was calculated by multiplying the percentage of infected RBCs by the total number of RBCs per unit volume
    - multiplying percent parasitemia by blood cell concentration
    - by qPCR
  - They did k-Means analysis, nearest neighbour to find trajectories in the phase plane
    - One such trajectory that we could access data for was the Parasite-Density to RBC Volume

## Slide 13 (Fitting Method)
- We took the meta data associated with their genetic sampling which contained parasite density and rbc volume
- Normalised and thresholded it for ease of fitting
- Integrated the model to find its trajectory
- Search for minima of an objective function for least squares using the Nelder-Mead algorithm (will see the obj. fn. later)

## Slide 14 (Regularisation)
- The minimisation is a least squares (minimising the two norm between the data vector and the trajectory vector)
- One problem was that even with a simple model, it's hard to find a unique set of parameters that fits the datad well
- Decided to try and apply regularisation techniques in an engineering sledgehammer way to the problem to reduce the number of parameters
  - In lieu of a formal parameter sensitivity analysis
- Tikhonov-inspired regularisation term to treat the problem
  - Penalise the objective function for deviating from a reference parameter vector which we would interpret as a do-nothing parameter vector
  - Idea was to remove additive or multiplicative interactions between parameters
    - Likely due to the way that the classification diagram from the modelling section showed (f/gy)

## Slide 15 16 17 (Diagrams)
- 0 for additive
- 1 for multiplicative
- 0/1 to allow terms to remove themselves from either
  - Abandoned since nothing really disappeared

## Slide 18 (Anova)
- Nelder-Mead Algo is a heuristic algorithm, wanted to see if the algorithm was hugely sensitive to initial seed
  - Seems like it is
  - This is a bad way of analysing variance
  - Hard to tell if fits are good
  - Can't determine a confidence interval that's useful

## Slide 19 (Resampling)
- Resampling
  - Idea is to remove data points at random from a data vector, and see if the fits are significantly different

## Slide 20 (Resampling Plot)
- Randomly resampled ~30 times per data vector
- Much less variance
- Also see that there's only one parameter that's significantly different
  - it's f
  - the immune response effect on health

## Slide 21 (Model Summary)
- Show that a simple model can reproduce very rich behaviours
- That point about (C) might show that the model needs to capture some other regulatory behaviour to be complete

## Slide 22 (FIt Summary)
- Process is so easy an engineer could come up with it
- Resampling an parameter reduction
- Immune response may be critical

## Slide 23 (Thanks)
- Supervisors (Vinod and Oliver)
- My contact
- Questions
